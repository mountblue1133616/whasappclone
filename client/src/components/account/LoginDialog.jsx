import { GoogleLogin } from "@react-oauth/google";
import jwt_decode from "jwt-decode";
import { Dialog, Box, Typography, List, ListItem, styled } from "@mui/material";
import { qrCodeImage } from "../../constants/data";
import { useContext } from "react";
import { AccountContext } from "../../context/AccountProvider";
import { addUser } from "../../service/api.js";

const dialogStyle = {
  height: "96%",
  marginTop: "12%",
  width: "60%",
  maxWidth: "100%",
  maxHeight: "100%",
  boxShadow: "none",
  overflow: "hidden",
};

const Component = styled(Box)`
  display: flex;
`;

const Container = styled(Box)`
  padding: 56px 0 56px 56px;
`;

const QRcode = styled("img")({
  height: 264,
  width: 264,
  margin: "50px 0 0 50px",
});

const Title = styled(Typography)`
  font-size: 26px;
  color: #525252;
  font-weight: 300;
  font-family: inherit;
  margin-bottom: 22px;
`;

const StyledList = styled(List)`
  & > li {
    padding: 0;
    margin-top: 15px;
    font-size: 18px;
    line-height: 28px;
    color: #4a4a4a;
  }
`;

const LoginDialog = () => {
  const { setAccount } = useContext(AccountContext);

  const onLoginSuccess = async (res) => {
    let decoded = jwt_decode(res.credential);
    setAccount(decoded);

    await addUser(decoded);
  };

  const onLoginFailure = (res) => {
    console.log("Login Failed:", res);
  };

  return (
    <Dialog open={true} PaperProps={{ sx: dialogStyle }} hideBackdrop={true}>
      <Component>
        <Container>
          <Title>To use WhatsApp on your computer:</Title>
          <StyledList>
            <ListItem>1. Open WhatsApp on your computer.</ListItem>
            <ListItem>
              2. Tap Menu or Settings and select Linked devices.
            </ListItem>
            <ListItem>3. Tap on Link a Device.</ListItem>
            <ListItem>
              4. Point your phone at this screen to capture the QR code.
            </ListItem>
          </StyledList>
        </Container>
        <Box>
          <QRcode src={qrCodeImage} alt="QR code" />
        </Box>
        <GoogleLogin
          buttonText=""
          onSuccess={onLoginSuccess}
          onError={onLoginFailure}
        />
      </Component>
    </Dialog>
  );
};

export default LoginDialog;
