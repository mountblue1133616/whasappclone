/* eslint-disable react/no-unescaped-entities */
import {
  Container,
  TextField,
  Button,
  Grid,
  Typography,
  styled,
} from "@mui/material";
import { useState } from "react";

const RootContainer = styled(Container)({
  marginTop: "10rem",
});

const Form = styled("form")({
  width: "100%",
  marginTop: "2rem",
});

const SubmitButton = styled(Button)({
  margin: "2rem 0 1rem",
  backgroundColor: "#00bfa5",
});

const LoginForm = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [picture, setPicture] = useState("");
  const [isSignUp, setIsSignUp] = useState(false);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handlePictureChange = (event) => {
    setPicture(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (isSignUp) {
      // Perform signup logic here
      console.log("Name:", name);
      console.log("Email:", email);
      console.log("Password:", password);
      console.log("Picture:", picture);
    } else {
      // Perform login logic here
      console.log("Email:", email);
      console.log("Password:", password);
    }
  };

  const handleFormSwitch = (event) => {
    event.preventDefault();
    setIsSignUp(!isSignUp);
  };

  return (
    <RootContainer component="main" maxWidth="xs">
      <div>
        <Typography component="h1" variant="h5" align="center">
          {isSignUp ? "Sign Up" : "Sign In"}
        </Typography>
        <Form onSubmit={handleSubmit}>
          {isSignUp && (
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="name"
              label="Name"
              name="name"
              autoComplete="name"
              value={name}
              onChange={handleNameChange}
            />
          )}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            value={email}
            onChange={handleEmailChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onChange={handlePasswordChange}
          />
          {isSignUp && (
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="picture"
              label="Profile Picture URL"
              name="picture"
              autoComplete="picture"
              value={picture}
              onChange={handlePictureChange}
            />
          )}
          <SubmitButton type="submit" fullWidth variant="contained">
            {isSignUp ? "Sign Up" : "Sign In"}
          </SubmitButton>
          <Grid container justify="flex-end">
            <Grid item>
              <Typography variant="body2">
                {isSignUp
                  ? "Already have an account?"
                  : "Don't have an account?"}
                <Button onClick={handleFormSwitch} color="primary">
                  {isSignUp ? "Sign In" : "Sign Up"}
                </Button>
              </Typography>
            </Grid>
          </Grid>
        </Form>
      </div>
    </RootContainer>
  );
};

export default LoginForm;
