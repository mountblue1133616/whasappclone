/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
import { Box, styled } from "@mui/material";
import Message from "./Message";

import Footer from "./Footer";
import { newMessages, getMessages } from "../../../service/api";
import { useContext, useState, useEffect } from "react";
import { AccountContext } from "../../../context/AccountProvider";

const Wrapper = styled(Box)`
  background-image: url(${"https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png"});
  background-size: 100%;
`;
const Component = styled(Box)`
  height: 80vh;
  overflow-y: scroll;
`;
const Messages = ({ person, conversation }) => {
  const [messages, setMessages] = useState([]);
  const [value, setValue] = useState();
  const { account, newMessageFlag, setNewMessageFlag } =
    useContext(AccountContext);
  const sendText = async (e) => {
    let code = e.keyCode || e.which;
    if (!value) return;

    if (code === 13) {
      let message = {
        senderId: account.sub,
        receiverId: person.sub,
        conversationId: conversation._id,
        type: "text",
        text: value,
      };
      await newMessages(message);
      setValue("");
      setNewMessageFlag((prev) => !prev);
    }
  };
  useEffect(() => {
    const getMessageDetails = async () => {
      let data = await getMessages(conversation?._id);
      setMessages(data);
    };
    getMessageDetails();
  }, [conversation?._id, person._id, newMessageFlag]);
  return (
    <Wrapper>
      <Component>
        {messages && messages.map((message) => <Message message={message} />)}
      </Component>
      <Footer sendText={sendText} value={value} setValue={setValue} />
    </Wrapper>
  );
};

export default Messages;
