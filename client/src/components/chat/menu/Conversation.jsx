/* eslint-disable react/prop-types */
import { useContext, useEffect, useState } from "react";

import { styled, Box, Typography } from "@mui/material";

import { AccountContext } from "../../../context/AccountProvider";
import { emptyProfilePicture } from "../../../constants/data";
import { setConversation } from "../../../service/api";

const Component = styled(Box)`
  height: 45px;
  display: flex;
  padding: 13px 0;
  cursor: pointer;
`;

const Image = styled("img")({
  width: 50,
  height: 50,
  objectFit: "cover",
  borderRadius: "50%",
  padding: "0 14px",
});

const Container = styled(Box)`
  display: flex;
`;

const Text = styled(Typography)`
  display: block;
  color: rgba(0, 0, 0, 0.6);
  font-size: 14px;
`;

const Conversation = ({ user }) => {
  const account = useContext(AccountContext);
  const url = user.picture || emptyProfilePicture;
  const { setPerson } = useContext(AccountContext);
  console.log(account);

  const getUser = async () => {
    setPerson(user);
    await setConversation({
      senderId: account.account.sub,
      receiverId: user.sub,
    });
    console.log(account.account.sub, "account sub");
  };

  return (
    <Component onClick={() => getUser()}>
      <Box>
        <Image src={url} alt="display picture" />
      </Box>
      <Box style={{ width: "100%" }}>
        <Container>
          <Typography>{user.name}</Typography>
        </Container>
        <Box>
          <Text>Hey</Text>
        </Box>
      </Box>
    </Component>
  );
};

export default Conversation;
