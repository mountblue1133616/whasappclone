/* eslint-disable no-undef */
import { MoreVert } from "@mui/icons-material";
import { Menu, MenuItem, styled } from "@mui/material";
import { useState } from "react";

const HeaderOptions = () => {
  const [open, setOpen] = useState(null);
  const handleClose = () => {
    setOpen(null);
  };
  const MenuOptions = styled(MenuItem)`
    font-size: 14px;
    padding: 15px 60px 5px 24px;
    color: #4a4a4a;
  `;
  const handleClick = (e) => {
    setOpen(e.currentTarget);
  };
  return (
    <>
      <MoreVert onClick={handleClick} />
      <Menu
        id="basic-menu"
        anchorEl={open}
        keepMounted
        open={open}
        onClose={handleClose}
        getContentAnchorE1={null}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <MenuOptions onClick={handleClose}>Profile</MenuOptions>
        <MenuOptions onClick={handleClose}>My account</MenuOptions>
        <MenuOptions onClick={handleClose}>Logout</MenuOptions>
      </Menu>
    </>
  );
};

export default HeaderOptions;
