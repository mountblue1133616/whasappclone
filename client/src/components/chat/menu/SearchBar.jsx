import { Box } from "@mui/material";
import { Search } from "@mui/icons-material";
import { InputBase } from "@mui/material";
import styled from "@emotion/styled";

const Component = styled(Box)`
  background-color: #fff;
  height: 45px;
  border-bottom: 1px solid #f2f2f2;
  display: flex;
  align-items: center;
`;

const InputField = styled(InputBase)`
width:100%,
padding:16px;
height:15px;
padding-left:65px;
font-size:14px
`;

const Icon = styled(Box)`
  position: absolute;
  height: 100%;
  padding: 3px 9px;
  color: #919191;
`;
const Wrapper = styled(Box)`
  background-color: #f0f2f5;
  margin: 0 13px;
  width: 100%;
  border-radius: 10px;
`;
const SearchBar = ({ setText }) => {
  return (
    <Component>
      <Wrapper>
        <Icon>
          <Search fontSize="small" />
        </Icon>
        <InputField
          placeholder="Search or start new chat"
          inputProps={{ "aria-label": "search" }}
          onChange={(e) => setText(e.target.value)}
        />
      </Wrapper>
    </Component>
  );
};

export default SearchBar;
