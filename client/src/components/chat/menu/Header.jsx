import { useContext, useState } from "react";
import { AccountContext } from "../../../context/AccountProvider";
import { Box, styled } from "@mui/material";
import { Chat, DonutLarge } from "@mui/icons-material";
import HeaderOptions from "./HeaderOptions";
import InfoDrawer from "../../drawer/InfoDrawer";

const Component = styled(Box)`
  height: 44px;
  background: #ededed;
  padding: 8px 16px;
  display: flex;
  align-items: center;
`;
const Image = styled("img")({
  height: 40,
  width: 40,
  borderRadius: "50%",
});
const IconWrapper = styled(Box)`
  margin-left: auto;
  & > * {
    margin-left: 2px;
    padding: 8px;
    color: #000;
  }
  &: first-child {
    font-size: 22px;
    margin-right: 8px;
    margin-top: 3px;
  }
`;

const Header = () => {
  const [openDrawer, setOpenDrawer] = useState(false);
  const { account } = useContext(AccountContext);

  console.log(account);
  const toggleDrawer = () => {
    setOpenDrawer(true);
    console.log("Image opened");
  };
  return (
    <>
      <Component>
        <Image
          src={account.picture}
          alt="Profile Picture"
          onClick={() => toggleDrawer()}
        />
        <IconWrapper>
          <DonutLarge />
          <Chat />
          <HeaderOptions />
        </IconWrapper>
      </Component>
      <InfoDrawer open={openDrawer} setOpen={setOpenDrawer} profile={true} />
    </>
  );
};

export default Header;
