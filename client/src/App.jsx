import "./App.css";
import Messenger from "./components/Messenger";
import AccountProvider from "./context/AccountProvider";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ChatDialog from "./components/chat/ChatDialog";
import { GoogleOAuthProvider } from "@react-oauth/google";

function App() {
  const clientId = `1077940674291-lt3vd567cl8ltm4d3aojqdjk342j0mut.apps.googleusercontent.com`;
  return (
    <GoogleOAuthProvider clientId={clientId}>
      <AccountProvider>
        <Router>
          <Routes>
            <Route path="/" element={<Messenger />} />
            <Route path="/user/:id" element={<ChatDialog />} />
          </Routes>
        </Router>
      </AccountProvider>
    </GoogleOAuthProvider>
  );
}

export default App;
